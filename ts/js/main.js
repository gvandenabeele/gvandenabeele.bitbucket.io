var Bullet = (function () {
    function Bullet(mg_left) {
        this.render(mg_left);
    }
    Bullet.prototype.render = function (mg_left) {
        var _this = this;
        this.bullet = document.createElement("div");
        this.bullet.id = "enemy";
        this.bullet.style.marginLeft = "50%";
        this.bullet.style.marginTop = "81%";
        this.bullet.style.transitionDuration = "2s";
        document.getElementById("container").appendChild(this.bullet);
        window.setTimeout(function () {
            _this.bullet.style.marginLeft = mg_left / (1365 / 100) + "%";
            _this.bullet.style.marginTop = "0%";
        }, 10);
    };
    Object.defineProperty(Bullet.prototype, "boundRec", {
        get: function () {
            return this.bullet.getBoundingClientRect;
        },
        enumerable: true,
        configurable: true
    });
    return Bullet;
}());
var Enemy = (function () {
    function Enemy() {
        this.CSS_COLOR_NAMES = [];
        this.CSS_COLOR_NAMES = ["AliceBlue", "Aqua", "Aquamarine", "Azure", "Beige", "Bisque", "Black", "BlanchedAlmond", "Blue", "BlueViolet", "Brown", "BurlyWood", "CadetBlue", "Chartreuse", "Chocolate", "Coral", "CornflowerBlue", "Cornsilk", "Crimson", "Cyan", "DarkBlue", "DarkCyan", "DarkGoldenRod", "DarkGray", "DarkGrey", "DarkGreen", "DarkKhaki", "DarkMagenta", "DarkOliveGreen", "Darkorange", "DarkOrchid", "DarkRed", "DarkSalmon", "DarkSeaGreen", "DarkSlateBlue", "DarkSlateGray", "DarkSlateGrey", "DarkTurquoise", "DarkViolet", "DeepPink", "DeepSkyBlue", "DimGray", "DimGrey", "DodgerBlue", "FireBrick", "ForestGreen", "Fuchsia", "Gainsboro", "Gold", "GoldenRod", "Gray", "Grey", "Green", "GreenYellow", "HoneyDew", "HotPink", "IndianRed", "Indigo", "Ivory", "Khaki", "Lavender", "LavenderBlush", "LawnGreen", "LemonChiffon", "LightBlue", "LightCoral", "LightCyan", "LightGoldenRodYellow", "LightGray", "LightGrey", "LightGreen", "LightPink", "LightSalmon", "LightSeaGreen", "LightSkyBlue", "LightSlateGray", "LightSlateGrey", "LightSteelBlue", "LightYellow", "Lime", "LimeGreen", "Linen", "Magenta", "Maroon", "MediumAquaMarine", "MediumBlue", "MediumOrchid", "MediumPurple", "MediumSeaGreen", "MediumSlateBlue", "MediumSpringGreen", "MediumTurquoise", "MediumVioletRed", "MidnightBlue", "MintCream", "MistyRose", "Moccasin", "Navy", "OldLace", "Olive", "OliveDrab", "Orange", "OrangeRed", "Orchid", "PaleGoldenRod", "PaleGreen", "PaleTurquoise", "PaleVioletRed", "PapayaWhip", "PeachPuff", "Peru", "Pink", "Plum", "PowderBlue", "Purple", "Red", "RosyBrown", "RoyalBlue", "SaddleBrown", "Salmon", "SandyBrown", "SeaGreen", "SeaShell", "Sienna", "Silver", "SkyBlue", "SlateBlue", "SlateGray", "SlateGrey", "Snow", "SpringGreen", "SteelBlue", "Tan", "Teal", "Thistle", "Tomato", "Turquoise", "Violet", "Wheat", "Yellow", "YellowGreen"];
        this.render();
    }
    Enemy.prototype.render = function () {
        var _this = this;
        var rnd = Math.floor((Math.random() * 100) + 1);
        var rnd_spd = Math.floor((Math.random() * 6) + 2);
        var rnd_clr = Math.floor((Math.random() * this.CSS_COLOR_NAMES.length));
        this.enemy = document.createElement("div");
        this.enemy.id = "enemy";
        this.enemy.style.marginLeft = rnd + "%";
        this.enemy.style.transitionDuration = rnd_spd + "s";
        this.enemy.style.backgroundColor = this.CSS_COLOR_NAMES[rnd_clr];
        document.getElementById("container").appendChild(this.enemy);
        window.setTimeout(function () {
            _this.enemy.style.marginLeft = "50%";
            _this.enemy.style.marginTop = "82%";
        }, 50);
        window.setInterval(function () {
            var towertop = document.getElementById("tower").getBoundingClientRect().top;
            var enemytop = _this.enemy.getBoundingClientRect().top;
            if (enemytop > (towertop - 10)) {
                _this.enemy.parentNode.removeChild(_this.enemy);
            }
        }, 20);
    };
    Object.defineProperty(Enemy.prototype, "enemyInstance", {
        get: function () {
            return this.enemy;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Enemy.prototype, "boundRec", {
        get: function () {
            return this.enemy.getBoundingClientRect;
        },
        enumerable: true,
        configurable: true
    });
    return Enemy;
}());
var app;
(function () {
    var init = function () {
        app = new Main();
    };
    window.addEventListener('load', init);
})();
var Main = (function () {
    function Main() {
        var _this = this;
        this.checkOverlap = function () {
            window.setInterval(function () {
                if (typeof _this.tower.theBullet !== 'undefined') {
                    var bullet_1 = _this.tower.theBullet.boundRec;
                    _this.enemy_array.forEach(function (element) {
                        var enemy = element.boundRec;
                        var overlap = !(bullet_1.right < enemy.left ||
                            bullet_1.left > enemy.right ||
                            bullet_1.bottom < enemy.top ||
                            bullet_1.top > enemy.bottom);
                        if (overlap == false) {
                            console.log("collison");
                        }
                    });
                }
            }, 10);
        };
        this.enemy_array = [];
        this.spawnTower();
        this.spawnEnemy();
        this.removeEnemy();
        this.checkOverlap();
    }
    Main.prototype.spawnTower = function () {
        this.tower = new Tower();
    };
    Main.prototype.spawnEnemy = function () {
        var _this = this;
        window.setInterval(function () {
            _this.enemy = new Enemy();
            _this.enemy_array.push(_this.enemy);
        }, 1);
    };
    return Main;
}());
var Tower = (function () {
    function Tower() {
        document.addEventListener("click", this.defender);
    }
    Tower.prototype.defender = function (event) {
        this.bullet = new Bullet(event.clientX);
    };
    Object.defineProperty(Tower.prototype, "theBullet", {
        get: function () {
            return this.bullet;
        },
        enumerable: true,
        configurable: true
    });
    return Tower;
}());
//# sourceMappingURL=main.js.map