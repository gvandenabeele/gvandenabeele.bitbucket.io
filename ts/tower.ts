/// <reference path="main.ts" />
class Tower{
    private bullet: Bullet;
        constructor(){
            document.addEventListener("click", this.defender);
        }

        //Creates bullet on mouse event
        defender(event:MouseEvent){
            this.bullet = new Bullet(event.clientX);
        }
        
        //Return bullet instance
        get theBullet(): Bullet{
            return this.bullet;
        }
    }

    