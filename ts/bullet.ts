
class Bullet{
    private bullet: HTMLDivElement;

        constructor(mg_left: number){
            this.render(mg_left);
            
        }
        //Render function for bullet object
        render(mg_left: number){
            this.bullet = document.createElement("div");
            this.bullet.id = "enemy";
            this.bullet.style.marginLeft = "50%";
            this.bullet.style.marginTop = "81%";
            this.bullet.style.transitionDuration = "2s";
            document.getElementById("container").appendChild(this.bullet);
            
            //Calculates bullet movement
            window.setTimeout(()=> { 
                this.bullet.style.marginLeft = mg_left/(1365/100)+"%";
                this.bullet.style.marginTop = "0%";
             }, 10 );      
        }

        //Get boundrec bullet
        get boundRec(): any{
            return this.bullet.getBoundingClientRect;
        }

    }

    