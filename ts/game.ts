let app : any;

(function ()
{
    /**
     * run after dom is ready
     */
    let init = function () 
    {
        app = new Main();

    };
    

    window.addEventListener('load', init);

})();