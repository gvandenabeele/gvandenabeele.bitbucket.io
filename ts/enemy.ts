class Enemy{
    private CSS_COLOR_NAMES: string[] = [];
    private enemy: HTMLDivElement;

        constructor(){
            this.CSS_COLOR_NAMES = ["AliceBlue","Aqua","Aquamarine","Azure","Beige","Bisque","Black","BlanchedAlmond","Blue","BlueViolet","Brown","BurlyWood","CadetBlue","Chartreuse","Chocolate","Coral","CornflowerBlue","Cornsilk","Crimson","Cyan","DarkBlue","DarkCyan","DarkGoldenRod","DarkGray","DarkGrey","DarkGreen","DarkKhaki","DarkMagenta","DarkOliveGreen","Darkorange","DarkOrchid","DarkRed","DarkSalmon","DarkSeaGreen","DarkSlateBlue","DarkSlateGray","DarkSlateGrey","DarkTurquoise","DarkViolet","DeepPink","DeepSkyBlue","DimGray","DimGrey","DodgerBlue","FireBrick","ForestGreen","Fuchsia","Gainsboro","Gold","GoldenRod","Gray","Grey","Green","GreenYellow","HoneyDew","HotPink","IndianRed","Indigo","Ivory","Khaki","Lavender","LavenderBlush","LawnGreen","LemonChiffon","LightBlue","LightCoral","LightCyan","LightGoldenRodYellow","LightGray","LightGrey","LightGreen","LightPink","LightSalmon","LightSeaGreen","LightSkyBlue","LightSlateGray","LightSlateGrey","LightSteelBlue","LightYellow","Lime","LimeGreen","Linen","Magenta","Maroon","MediumAquaMarine","MediumBlue","MediumOrchid","MediumPurple","MediumSeaGreen","MediumSlateBlue","MediumSpringGreen","MediumTurquoise","MediumVioletRed","MidnightBlue","MintCream","MistyRose","Moccasin","Navy","OldLace","Olive","OliveDrab","Orange","OrangeRed","Orchid","PaleGoldenRod","PaleGreen","PaleTurquoise","PaleVioletRed","PapayaWhip","PeachPuff","Peru","Pink","Plum","PowderBlue","Purple","Red","RosyBrown","RoyalBlue","SaddleBrown","Salmon","SandyBrown","SeaGreen","SeaShell","Sienna","Silver","SkyBlue","SlateBlue","SlateGray","SlateGrey","Snow","SpringGreen","SteelBlue","Tan","Teal","Thistle","Tomato","Turquoise","Violet","Wheat","Yellow","YellowGreen"];
            this.render();
        }
        
        //Render function for enemy
        public render(){
            //Randoms for dom creation
            let rnd = Math.floor((Math.random() * 100) + 1);
            let rnd_spd = Math.floor((Math.random() * 6) +2)
            let rnd_clr = Math.floor((Math.random() * this.CSS_COLOR_NAMES.length))

            //Dom creation
            this.enemy = document.createElement("div");
            this.enemy.id = "enemy";
            this.enemy.style.marginLeft = rnd+"%";
            this.enemy.style.transitionDuration = rnd_spd+"s";
            this.enemy.style.backgroundColor = this.CSS_COLOR_NAMES[rnd_clr];
            document.getElementById("container").appendChild(this.enemy);
            
            //Transform
            window.setTimeout(()=> { 
            this.enemy.style.marginLeft = "50%";
            this.enemy.style.marginTop = "82%";
             }, 50 );
            
            //Removes enemy on touching tower
            window.setInterval(()=> { 
            let towertop = document.getElementById("tower").getBoundingClientRect().top;
            let enemytop = this.enemy.getBoundingClientRect().top;
            if(enemytop>(towertop-10)){
                this.enemy.parentNode.removeChild(this.enemy);
            }
                }, 20 );
        }

        //Returns enmey instance
        get enemyInstance(){
            return this.enemy;
        }
        //Returns enemy boundingclientrect
        get boundRec(): any{
            return this.enemy.getBoundingClientRect;
        }
    }