class Main{
private tower: Tower;
private enemy: Enemy;
private spawncount: number;  
private enemy_array:Enemy[];

    constructor(){
        this.enemy_array = [];
        this.spawnTower();
        this.spawnEnemy();
        this.removeEnemy();
        this.checkOverlap();
    }

    //Spawn tower
    public spawnTower(){
        this.tower = new Tower();
    }
    //Spawn tower loop
    public spawnEnemy(){
        window.setInterval(()=>{
            this.enemy = new Enemy();
            this.enemy_array.push(this.enemy);
          }, 1);
    }


    //Collision check 
    public checkOverlap = () =>{
        window.setInterval(()=>{
            
            if(typeof this.tower.theBullet !== 'undefined'){
                let bullet = this.tower.theBullet.boundRec;
                
                this.enemy_array.forEach(element => {
                    let enemy = element.boundRec;
                    let overlap = !(bullet.right < enemy.left || 
                        bullet.left > enemy.right || 
                        bullet.bottom < enemy.top || 
                        bullet.top > enemy.bottom)
                    if(overlap == false){
                        console.log("collison")
                        //element.parentNode.removeChild(element);
                    }
                }); 
            }
            
          }, 10);
    }
}